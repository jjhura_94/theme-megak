import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { ServiceDialogComponent } from './service-dialog/service-dialog.component';



@NgModule({
  declarations: [
    HomeComponent,
    ServiceDialogComponent
  ],
  imports: [
    CommonModule
  ]
})
export class HomeModule { }
