import { HomeModule } from './home/home.module';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{
  path: '',
  component : HomeComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes), HomeModule],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
