import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, Input, OnInit } from '@angular/core';

declare function initCicurlarMenu1():any;

declare function openCircleItem():any;

@Component({
  selector: 'app-service-dialog',
  templateUrl: './service-dialog.component.html',
  styleUrls: ['./service-dialog.component.scss']
  
})
export class ServiceDialogComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  @Input()
  typeService: any = "1";

  async ngOnInit(): Promise<void> {
    initCicurlarMenu1();
    await this.delay(300);
    openCircleItem();
  }

  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  closeModal(){
    this.modalService.dismissAll();
  }

}
