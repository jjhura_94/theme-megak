import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ServiceDialogComponent } from './service-dialog/service-dialog.component';
import { AfterViewInit, Component, OnInit } from '@angular/core';
declare function mobileMenuBtn():any;
declare function initSlide():any;
declare function initCicurlarMenu(id:any,x:number,y:number):any;
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  typeService: any = 1;

  constructor(
    private modalService: NgbModal
  ) { }
  ngAfterViewInit(): void {
    mobileMenuBtn();
    initSlide()
    // initCicurlarMenu("navs",-1,-1);
    // initCicurlarMenu("navs1",1,-1);
    // initCicurlarMenu("navs2",-1,1);
    // initCicurlarMenu("navs3",1,1);
  }

  ngOnInit(): void {
  }

  showDialog  = false;
  openDialog(): void {
    this.showDialog = true;
  }

  closeResult: string = '';

  open(content:any, typeService: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title',centered:true,size:'lg',windowClass:'modal-content-custom'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    this.typeService = typeService;
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
